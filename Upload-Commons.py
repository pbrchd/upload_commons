#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
################################################################################
 @brief Wikimedia Commons JPEG Bulk Upload Tool

 @author: Wojciech Migda (Wmigda)

 @license: GPLv3: http://www.gnu.org/licenses/gpl-3.0.html

 Based on Nichalp's Upload Script
 http://commons.wikimedia.org/wiki/User:Nichalp/Upload_script

 To be used in association with Nichalp's csv_create.pl script as a
 replacement of upload.pl

################################################################################
 History:
 Date         Who     Description
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 2010-Jul-04  wmigda  Support for command line options
 2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable through
                      command line options
 2010-Jul-07  wmigda  BUG602827 - EXIF tags embedding is unconditional
################################################################################
"""

import os
import sys
import re
import cookielib
import csv          # http://docs.python.org/library/csv.html
import random       # http://docs.python.org/library/random.html
import getpass      # http://docs.python.org/library/getpass.html
import optparse     # http://docs.python.org/library/optparse.html
import tempfile     # http://docs.python.org/library/tempfile.html
import subprocess   # http://docs.python.org/library/subprocess.html

try:
    import mechanize    # http://wwwsearch.sourceforge.net/mechanize/
except ImportError:
    print "Required 'mechanize' module cannot be found. Aborting."
    sys.exit(1)

"""
################################################################################

    Python Singleton implementation using metaclass programming
    
    Ref:
        http://en.wikipedia.org/wiki/Singleton_pattern#Python

################################################################################
"""
class SingletonType(type):
    def __call__(cls):
        if getattr(cls, '__instance__', None) is None:
            instance = cls.__new__(cls)
            instance.__init__()
            cls.__instance__ = instance
        return cls.__instance__

"""
################################################################################

    This is a global configuration singleton class.

    To create an instance:
        gconf = gConf()

    To add an attribute:
        gconf.add_param('magic_number', 47)
    
    To access the attribute:
        print gconf.magic_number
    
    To change attribute:
        gconf.magic_number = 83

################################################################################
"""
class gConf:
    # singleton, see en.wiki
    __metaclass__ = SingletonType

    def add_param(self, name, value):
        setattr(self, name, value)
        return
    pass
### gConf ######################################################################

"""
################################################################################
 @brief Command line parameters global access class
################################################################################
 History:
 Date         Who     Description
 -------------------------------------------------------------------------------
 2010-Jul-04  wmigda  Initial version
 2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable through
                      command line options
 -------------------------------------------------------------------------------
 With first instance creation the parser is invoked.
 Then 'options' and 'args' are available as class attributes
################################################################################
"""
class clParms:
    # singleton, see en.wiki
    __metaclass__ = SingletonType
    
    def __init__(self):
        self.options = None
        self.args = None

        self.parser = optparse.OptionParser(usage = "usage: %prog [options]", version = "%prog 0.0.2")

        self.parser.add_option("--scan", dest="scanImages", action="store_true", default=False, help="scan images in the current diretory and create initial CSV file. Default name of the CSV file can be changed with the --images-csv option.")
        self.parser.add_option("--images-csv", dest="fnCsvImages", type="string", default='upload.csv', help="name of CSV file with files to upload", metavar="FILE")
        self.parser.add_option("--mediawiki-domain", dest="mediawikiDomain", type="string", default='commons.wikimedia.org', help="domain name of the mediawiki to upload files to, defaults to 'commons.wikimedia.org'.", metavar="DOMAIN")

        (self.options, self.args) = self.parser.parse_args()
    pass
### clParms ####################################################################


"""
################################################################################
 @brief Browser class to access Wikimedia Commons
################################################################################
 History:
 Date         Who     Description
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable through
                      command line options
################################################################################
"""
class CommonsBrowser:
    br = None
    cj = None
    domain = None
    
    """
    ############################################################################
     @brief Constructor
    ############################################################################
     History:
     Date         Who     Description  
     ---------------------------------------------------------------------------
     2010-Jun-21  wmigda  Initial version
     2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable
                          through command line options
     ---------------------------------------------------------------------------
     Based on:
     http://stockrt.github.com/p/emulating-a-browser-in-python-with-mechanize/
    ############################################################################
    """
    def __init__(self):
        # Browser
        self.br = mechanize.Browser()
        
        # Cookie Jar
        self.cj = cookielib.LWPCookieJar()
        self.br.set_cookiejar(self.cj)
        
        # Browser options
        self.br.set_handle_equiv(True)
        #self.br.set_handle_gzip(True)
        self.br.set_handle_redirect(True)
        self.br.set_handle_referer(True)
        self.br.set_handle_robots(False)
        
        # Follows refresh 0 but not hangs on refresh > 0
        self.br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
        
        # Want debugging messages?
        #self.br.set_debug_http(True)
        #self.br.set_debug_redirects(True)
        #self.br.set_debug_responses(True)
        
        # User-Agent (this is cheating, ok?)
        self.br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

        # remember mediawiki domain name setting from command line
        clparms = clParms()
        self.domain = clparms.options.mediawikiDomain
    ### __init__ ###############################################################


    """
    ############################################################################
     @brief Login into Wikimedia Commons using provided credentials
    ############################################################################
     History:
     Date         Who     Description  
     ---------------------------------------------------------------------------
     2010-Jun-21  wmigda  Initial version
     2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable
                          through command line options
     ---------------------------------------------------------------------------
     @param wpLogin Wikimedia Commons user name
     @param wpPassword Wikimedia Commons user password
    
     @return tuple of status code and browser response
     ---------------------------------------------------------------------------
     TODO:
     - save cookies for reuse
     - 
    ############################################################################
    """
    def login(self, wpLogin, wpPassword):
        self.br.open("http://%s/wiki/Special:UserLogin" % (self.domain))
        self.br.select_form(name="userlogin")
        
        self.br["wpName"] = wpLogin
        self.br["wpPassword"] = wpPassword
        # submit form,
        try:
            re = self.br.submit()
            status = re.code
        except HTTPError, e:
            print "Mechanize error: code=%" % e.code
            return (e.code, None)
        return (status, re)
    ### login ##################################################################


    """
    ############################################################################
     @brief Login into Wikimedia Commons using provided credentials
    ############################################################################
     History:
     Date         Who     Description  
     ---------------------------------------------------------------------------
     2010-Jun-21  wmigda  Initial version
     2010-Jul-05  wmigda  BUG602008 - Make mediawiki domain name selectable
                          through command line options
     ---------------------------------------------------------------------------
     @param localName local file name to read from
     @param commonsName destination file name on Wikimedia Commons
     @param metadata metadata to attach to the uploaded file
    
     @return tuple of status code and browser response
     ---------------------------------------------------------------------------
     TODO:
     - 
    ############################################################################
    """
    def upload(self, localName, commonsName, metadata):
        self.br.open("http://%s/wiki/Special:Upload" % (self.domain))
        self.br.select_form(nr = 0)
        
        self.br.add_file(open(localName), 'text/plain', localName, name="wpUploadFile")
        #self.br["wpUploadFile"] = localName
        self.br["wpDestFile"] = commonsName
        self.br["wpUploadDescription"] = metadata
        #self.br["wpUploadAffirm"] = "1"
        #self.br["wpIgnoreWarning"] = "1"
        #self.br["wpWatchthis"] = "1"
        try:
            re = self.br.submit()
            status = re.code
        except HTTPError, e:
            print "Mechanize error: code=%" % e.code
            return (e.code, None)
        return (status, re)
    ### upload #################################################################


"""
################################################################################
 @brief Utility to check if the variable is a parsable integer
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 -------------------------------------------------------------------------------
 @param x variable to test

 @return @c True if argument variable converts to integer
################################################################################
"""
def isNumber(x):
    try:
        int(x)
        return True
    except TypeError:
        return False

    
"""
################################################################################
 @brief Convenience function to login to Wikimedia Commons
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 -------------------------------------------------------------------------------
 @param browser browser instance to use for login
 @param wpLogin Wikimedia Commons user name
 @param wpPassword Wikimedia Commons user password

 @return @c True if login was successful
################################################################################
"""
def tryLogin(browser, wpLogin, wpPassword):
    (status, response) = browser.login(wpLogin, wpPassword)
    if status != 200:
        return False
    else:
        # TODO: check for successful login: wgUserName!=null
        return True
### tryLogin ###################################################################


"""
################################################################################
 @brief Embed EXIF and IPTC tags from CSV row data
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jul-18  wmigda  Initial version
 -------------------------------------------------------------------------------
 @param fName name of the file to embed tags into
 @param row list of fields (row) read from CSV file
 -------------------------------------------------------------------------------
 This version supports exiv2 executable only. Future modifications shall include
 direct pyexiv2 usage as well if relevant module is available. 
################################################################################
"""
def embedExifFromRow(fName, row):
    gconf = gConf()

    if gconf.has_exiv2:
        # build Exiv2 command
        exiv2Cmd = \
"""\
set Exif.Image.Orientation 1
set Exif.Image.Artist %s
set Exif.Image.Copyright © %s %s, %s
set Exif.Image.ImageDescription %s
set Exif.Photo.UserComment %s
set Iptc.Envelope.CharacterSet \33%%G
set Iptc.Application2.RecordVersion 4
set Iptc.Application2.Writer %s
set Iptc.Application2.Copyright © %s %s, %s
set Iptc.Application2.Byline %s
set Iptc.Application2.BylineTitle editor
set Iptc.Application2.Caption %s
""" % (\
        row['Author name'], \
        row['Year'], row['Author name'], row['Exif perm'], \
        row['Description'], \
        ", ".join([row['Category1'], row['Category2'], row['Category3'], row['Category4'], row['Category5']]), \
        gconf.wpLogin, \
        row['Year'], row['Author name'], row['Exif perm'],
        row['Author name'], \
        row['Description']
        )

        # append IPTC keywords
        for keyword in [row['Category1'], row['Category2'], row['Category3'], row['Category4'], row['Category5']]:
            if len(keyword):
                exiv2Cmd += "add Iptc.Application2.Keywords %s\n" % (keyword)

        # add country, state/province, city, if they exist
        if row['Country'] != "":
            exiv2Cmd += "set Iptc.Application2.CountryName %s\n" % (row['Country'])
        if row['State'] != "":
            exiv2Cmd += "set Iptc.Application2.ProvinceState %s\n" % (row['State'])
        if row['Place'] != "":
            exiv2Cmd += "set Iptc.Application2.City %s\n" % (row['Place'])

        # append Source and Credit
        if row['Website'] != "":
            exiv2Cmd += "set Iptc.Application2.Source %s\n" % (row['Website'])
            exiv2Cmd += "set Iptc.Application2.Credit %s, %s\n" % (row['Author name'], row['Website'])
        else:
            exiv2Cmd += "set Iptc.Application2.Credit %s\n" % (row['Author name'])

        # append Headline
        if row['Caption'] != "":
            exiv2Cmd += "set Iptc.Application2.Headline %s\n" % (row['Caption'])
        
        # create temporary file and write its contents
        fCmd = tempfile.NamedTemporaryFile()
        fCmd.write(exiv2Cmd)
        fCmd.flush()
        os.fsync(fCmd)
        
        # execute exiv2
        os.system('exiv2 -m %s "%s"' % (fCmd.name, fName))
        
        # close temporary file, it will be removed automaticaly
        fCmd.close()
        
        print "Embedded EXIF/IPTC tags into %s" % (fName)
        pass
    return
### embedExifFromRow(row) ######################################################


"""
################################################################################
 @brief Process contents of a row with image data from the parsed CSV file
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 2010-Jul-07  wmigda  BUG602827 - EXIF tags embedding is unconditional
 -------------------------------------------------------------------------------
 @param row dictionary with row fields
 @param br browser instance to use for mediawiki access

 @return @c True if login was successful
################################################################################
"""
def processCsvRow(row, br):
    gconf = gConf()

    # integrity check
    # uppercase/lowercase conversions
    row['Lat Ref'] = row['Lat Ref'].decode('utf-8').upper()
    row['Long Ref'] = row['Long Ref'].decode('utf-8').upper()
    row['Region'] = row['Region'].decode('utf-8').upper()
    row['Camera info?'] = row['Camera info?'].decode('utf-8').upper()
    row['Embed exif?'] = row['Embed exif?'].decode('utf-8').upper()
    row['Type'] = row['Type'].decode('utf-8').lower()
    row['Coordinate type'] = row['Coordinate type'].decode('utf-8').lower()

    if row['Current name'] == "":
        print "'Current name' is empty - skipped"
        return
    print(row['Description'])
    if row['Description'] == "":
        print "Please add a description to " + row['Current name']
        return
    
    # Permissions
    if row['Permissions'] == "":
        print "Please enter the permission details to " + row['Current name']
        return
    if int(row['Permissions']) > 7 or int(row['Permissions']) < 0:
        print "Please enter a correct permissions value for " + row['Current name']
        return
    
    # Category check
    if row['Category1'] == "":
        print "Please enter a category to " + row['Current name']
        return
    
    # Latitude and longitude cannot exist without each other. So checking. If both do not exist, ignore, as the template is optional.
    '''
    if (row['Lat deg'] == "") ^ (row['Long deg'] == ""):
        if row['Lat deg'] == "":
            print "Please add the latitude to " + row['Current name']
        else:
            print "Please add the longitude to " + row['Current name']
        return
    
    # Checking dms values
    if row['Coordinate type'] == "":
        row['Coordinate type'] = "d"
    
    # Checking for valid values if both are entered
    if row['Lat deg'] != "" and row['Long deg'] != "":
        if row['Lat Ref'] == "":
            print "Please add the latitude reference (N or S) to " + row['Current name']
            return
        if row['Long Ref'] == "":
            print "Please add the longitude reference (E or W) to " + row['Current name']
            return
        if row['Lat Ref'] != "N" and row['Lat Ref'] != "S":
            print "Invalid latitude reference for " + row['Current name']
            return
        if row['Long Ref'] != "E" and row['Long Ref'] != "W":
            print "Invalid longitude reference for " + row['Current name']
            return
        if float(row['Lat deg']) < 0 or float(row['Lat deg']) > 90:
            print row['Lat deg'] + " is not a valid latitude for " + row['Current name']
            return
        if float(row['Long deg']) < -180 or float(row['Long deg']) > 180:
            print row['Long deg'] + " is not a valid longitude for " + row['Current name']
            return
       ''' 
    # Checking for valid geo_type parameters
    if row['Type'] != "":
        # http://en.wikipedia.org/wiki/Wikipedia:GEO#type:T
        if row['Type'] not in ["country", "satellite", "adm1st", "adm2nd", "adm3rd", "city", "airport", "mountain", "isle", "waterbody", "forest", "river", "glacier", "event", "edu", "pass", "railwaystation", "landmark"]:
            # for now city(pop) is invalid here :(, will need a regex
            print "Invalid 'Type' attributes for %s. Please enter a valid geotype." % (row['Current name'])
            return

    # Checking for geoheading issues
    if row['Heading'] != "":
        if row['Heading'].decode('utf-8').upper() not in ["N", "NBE", "NNE", "NEBN", "NE", "NEBE", "ENE", "EBN", "E", "EBS", "ESE", "SEBE", "SE", "SEBS", "SSE", "SBE", "S", "SBW", "SSW", "SWBS", "SW", "SWBW", "WSW", "WBS", "W", "WBN", "WBW", "NWBW", "NW", "NWBN", "NNW", "NBW"]:
            if isNumber(row['Heading']):
                if int(row['Heading']) < 0 or int(row['Heading']) > 360:
                    print "Invalid 'Heading' attribute for %s. Please enter a valid heading (0-360 or a compass point)." % (row['Current name'])
                    return
            else:
                print "Invalid 'Heading' attribute for %s. Please enter a valid heading (0-360 or a compass point)." % (row['Current name'])
                return

    # Checking for valid Xform (JPEG transform) parameters
    if row['Xform'] != "":
        if int(row['Xform']) not in range(1, 10) + [90, 180, 270]:
            print "Invalid transformation (Xform) attributes for %s. Please enter a valid number." % (row['Current name'])
            return
        
    # Integration
    
    # Creating the gallery text file
    try:
        fGal = open("gallery.txt", "w")
    except IOError:
        print "Cannot open 'gallery.txt' for writting."
        sys.exit(1)
    fGal.write("<gallery>\n")
    
    # Replace spaces in the picture filename
    row['Current name'] = re.sub('\s', '_', row['Current name'])
    
    # Calculating the altitude reference
    if row['Altitude'] != "":
        if int(row['Altitude']) < 0:
            row['Altitude Ref'] = "Below Sea Level"
        else:
            row['Altitude Ref'] = "Above Sea Level"
    
    # This checks to see if the [[Template:Location]] geo-box is needed, and fills in the values
    geo_arr = []
    if row['Lat deg'] != "" and row['Long deg'] != "":
        if row['Type'] != "":
            geo_arr += ["type:" + row['Type']]
        if row['Scale'] != "":
            geo_arr += ["scale:" + row['Scale']]
        if row['Region'] != "":
            geo_arr += ["region:" + row['Region']]
        if row['Heading'] != "":
            geo_arr += ["heading:" + row['Heading']]
        if row['Source'] != "":
            geo_arr += ["source:" + row['Source']]
        row['Geo Params'] = '_'.join(geo_arr)
        
        if row['Lat Ref'] == "S":
            # Negative values for southern hemisphere
            row['Lat deg'] = -abs(int(row['Lat deg']))
        if row['Long Ref'] == "W":
            # Negative values for western hemisphere
            row['Long deg'] = -abs(int(row['Long deg']))
        row['Geo Ref'] = "{{Location dec|%s|%s|%s}}\n" % \
                        (row['Lat deg'], row['Long deg'], row['Geo Params'])
        row['GPS Lat'] = row['Lat deg']
        #row['GPS Lat'] = abs(int(row['Lat deg']))
        row['GPS Long'] = row['Long deg']
        #row['GPS Long'] = abs(int(row['Long deg']))
        
        if row['Lat Ref'] == "S":
            row['GPS Lat Ref'] = "South"
        else:
            row['GPS Lat Ref'] = "North"
        if row['Long Ref'] == "E":
            row['GPS Long Ref'] = "East"
        else:
            row['GPS Long Ref'] = "West"
    
    # Author info
    if row['Author name'] == "":
        full_author = gconf.wpLogin
    else:
        #full_author = "%s ([[User:%s|%s]])" % (row['Author name'], gconf.wpLogin, gconf.wpLogin)
        full_author = "%s" % (row['Author name'])

    # Extracting the date
    row['Date'] = (row['Date'][0:10]).replace(":", "-")
    row['Year'] = row['Date'][0:4]
    
    # Permissions
    perms = \
        ["user defined",
         "{{self|cc-by-sa-3.0,2.5,2.0,1.0|GFDL}}",
         "{{self|cc-by-sa-3.0}}",
         "{{self|cc-by-sa-3.0,2.5,2.0,1.0}}",
         "{{self|cc-by-sa-3.0|GFDL}}",
         "{{PD-self}}",
         "{{self|cc-pd}}",
         "{{self|cc-by-3.0}}"]
    if row['Permissions'] == "0":
        # User defined permission
        # TODO
        license = "User defined permission - TODO"
    else:
        license = perms[int(row['Permissions'])]

    exif_perms = \
        ["user defined",
         "Released under the Creative Commons attribution and share-alike licences v1, 2, 2.5 and 3 and GFDL",
         "Released under Creative Commons attribution and share-alike licence v 3.0",
         "Released under the Creative Commons attribution and share-alike licences v1, 2, 2.5 and 3",
         "Released under Creative Commons licence attribution and share-alike v 3.0",
         "Released into Public Domain",
         "Released into public domain using the Creative Commons Public Domain Dedication",
         "Released under Creative Commons attribution licence v 3.0"]
    row['Exif perm'] = exif_perms[int(row['Permissions'])]
    
    rot_tmpl = ""
    if row['Xform'] != "":
        if row['Xform'] in ["90", "180", "270"]:
            rot_tmpl = "{{rotate|%s}}\n" % (row['Xform'])
    
    
    # Camera information: [[Template:Photo information]]
    photo_tmpl = ""
    if row['Camera info?'] == "Y":
        # Remove leading single quote
        row['Shutter'] = row['Shutter'][1:-1]
        print "Shutter value: %s" % (row['Shutter'])
        photo_tmpl = (\
            "{{Photo Information\n" + \
            "| Model\t\t= %s\n" + \
            "| Aperture\t= %s\n" + \
            "| Shutter\t= %s\n" + \
            "| Film\t\t= %s\n" + \
            "| ISO\t\t= %s\n" + \
            "| Lens\t\t= %s\n}}\n") % \
            (row['Camera Model'], row['Aperture'], row['Shutter'], row['Film'], row['ISO'], row['Lens'])

    # Formatting metadata
    
    # Formatting the Description parameter
    if row['Description lang'] == "":
        row['Description lang'] = "en"
    full_desc = "\n{{%s|%s}}" % (row['Description lang'], row['Description']);
    if row['Description lang 2'] != "" and row['Description 2'] != "":
        full_desc += "\n{{%s|%s}}" % (row['Description lang 2'], row['Description 2']);
    if row['Description lang 3'] != "" and row['Description 3'] != "":
        full_desc += "\n{{%s|%s}}" % (row['Description lang 3'], row['Description 3']);

    # Categories: Combining all categories as one master category
    master_cat = "[[Category:%s]]" % (row['Category1']);
    if row['Category2'] != "":
        master_cat += "\n[[Category:%s]]" % (row['Category2']);
    if row['Category3'] != "":
        master_cat += "\n[[Category:%s]]" % (row['Category3']);
    if row['Category4'] != "":
        master_cat += "\n[[Category:%s]]" % (row['Category4']);
    if row['Category5'] != "":
        master_cat += "\n[[Category:%s]]" % (row['Category5']);
    
    # Other versions
    other_vers = ""
    if row['Other versions 1'] != "":
        other_vers += "\n* [[:File:%s|%s]]" % (row['Other versions 1'], row['Other versions 1'])
    if row['Other versions 2'] != "":
        other_vers += "\n* [[:File:%s|%s]]" % (row['Other versions 2'], row['Other versions 2'])
    
    # Information template
    meta = (\
        "\n== {{int:filedesc}} ==\n\n" + \
        "{{Information\n" + \
        "| Description\t= %s\n" + \
        "| Source\t= %s\n" + \
        "| Author\t= %s\n" + \
        "| Date\t\t= %s\n" + \
        "| Permission\t= \n" + \
        "| Other versions= %s\n" + \
        "}}\n") % \
        (full_desc, row['Source'], full_author, row['Date'], other_vers);
    # Begin optional template integration
    if row.has_key('Geo Ref'):
        if row['Geo Ref'] != "":
            meta += row['Geo Ref'] + "\n"
    if photo_tmpl != "":
        meta += photo_tmpl + "\n"
    if rot_tmpl != "":
        meta += rot_tmpl + "\n"
    if row['Other information'] != "":
        meta += row['Other information'] + "\n"
    
    # Integrating remaining templates into one 
    meta += (\
        "\n== [[Commons:Copyright tags|Licensing]] ==\n" + \
        "%s\n\n\n" + \
        "%s\n" + \
        "\n") % \
        (license, master_cat)

    #print meta
    
    # Renaming and writing Exif data

    # Begin Exif addition
    if row['Embed exif?'] == 'Y':
        embedExifFromRow(row['Current name'] ,row)
        pass
    
    # convert spaces to underscores
    row['New name'] = re.sub('\s', '_', row['New name'])
    # Image manipulations (rotations, transformations, etc.)
    # I will use jpegtran just as the Nichalp's script, as it's best for this
    # PIL nor Imagemagick doesn't preserve JPEG's quality
    '''
    jpegtran_args = \
        {"2" : '-flip horizontal', 
         "3" : '-rotate 180', 
         "4" : '-flip vertical', 
         "5" : '-transpose', 
         "6" : '-rotate 90', 
         "7" : '-transverse', 
         "8" : '-rotate 270',
         "9" : '-greyscale'}
    if int(row['Xform']) in range(2, 10):
        print "Rotating %s into %s ..." % (row['Current name'], row['New name'])
        fnTemp = row['Current name'] + str(random.randint(1, 100000000)).zfill(8)
        os.system("jpegtran -copy all %s %s > %s" % (jpegtran_args[row['Xform']], row['Current name'], fnTemp));
        os.rename(fnTemp, row['New name'])
        os.remove(row['Current name'])
        print "Successful!"
    else:
        print "Renaming %s into %s ..." % (row['Current name'], row['New name'])
        os.rename(row['Current name'], row['New name'])
        print "Successful!"
    '''
    # Uploading images to commons
    print "Uploading %s to the Wikimedia Commons. \nDescription: %s" % (row['Current name'], row['Description'])
    print meta
    br.upload(row['New name'], row['New name'], meta)
    return
### processCsvRow ##############################################################


"""
################################################################################
 @brief Check system settings and capabilities
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jul-13  wmigda  Initial version
                      BUG602827 - EXIF tags embedding is unconditional
 -------------------------------------------------------------------------------
 Checks system for settings and capabilities (such as presence of executables)
 and stores the results in the global configuration singleton
################################################################################
"""
def checkSystem():
    gconf = gConf()
    
    # list of directories to look for user configuration files
    gconf.add_param('cfg_dir', [os.path.expanduser('~')])
    
    # check EXIF tools presence
    fTemp = tempfile.TemporaryFile()
    try:
        subprocess.call(['exiv2', '--version'], stderr=fTemp, stdout=fTemp)
        has_exiv2 = True
    except OSError:
        has_exiv2 = False
    fTemp.close()
    gconf.add_param('has_exiv2', has_exiv2)
    
    return
### checkSystem ################################################################

"""
################################################################################
 @brief Main function
################################################################################
 History:
 Date         Who     Description  
 -------------------------------------------------------------------------------
 2010-Jun-21  wmigda  Initial version
 2010-Jul-07  wmigda  BUG602827 - EXIF tags embedding is unconditional
 -------------------------------------------------------------------------------
 TODO
 - refactor to extract code into callable functions
 - writting into gallery file
 - entering user defined permissions
 - logging trace/debug into file
 - parse city(pop) for success
################################################################################
"""
def main():
    # setup some global configuration variables using gConf
    gconf = gConf()

    # parse command line options
    clparms = clParms()
    
    # determine system settings and capabilities
    checkSystem()

    # read Wikimedia Commons credentials from stdin
    try:
        wpLogin = raw_input("Login: ")
        wpPassword = getpass.getpass()
        gconf.add_param('wpLogin', wpLogin)
        gconf.add_param('wpPassword', wpPassword)
    except KeyboardInterrupt:
        print "\nCtrl-C pressed. Exiting..."
        sys.exit(0)
    except EOFError:
        print "\nCtrl-D pressed. Exiting..."
        sys.exit(0)

    # instantiate browser
    br = CommonsBrowser()

    # test Mediawiki login
    if not tryLogin(br, wpLogin, wpPassword):
        print "Cannot login into Wikimedia Commons with provided credentials!"
        sys.exit(1)

    # load the CSV file
    if not os.access(clparms.options.fnCsvImages, os.F_OK + os.R_OK):
        print "File %s doesn't exist or cannot be read. Exiting..." % (clparms.options.fnCsvImages)
        sys.exit(1)
    dict = csv.DictReader(open(clparms.options.fnCsvImages))

    for row in dict:
        processCsvRow(row, br)

    return
### main #######################################################################

if __name__ == "__main__" :
    main()
